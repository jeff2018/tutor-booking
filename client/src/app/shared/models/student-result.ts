export class StudentResult {
    constructor(
        public id: number,
        public scores: number,
        public level: string,
        public subject: string,
        public name: string
    ) {

    }
}