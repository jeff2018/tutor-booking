export class StudentCriteria {
    constructor(
        public keyword: string,
        public searchType: string,
        public currentPerPage: number,
        public itemsPerPage: number
    ){

    }
}