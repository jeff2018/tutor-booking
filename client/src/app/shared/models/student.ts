export class Student {
    constructor(
        public scores: number,
        public level: string,
        public subject: string,
        public name: string,
        public id?: number
    ){

    }
}