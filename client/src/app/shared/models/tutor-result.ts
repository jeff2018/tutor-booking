export class TutorResult {
    constructor(
        public id: number,
        public name: string,
        public location: string,
        public subject: string,
        public review: number,
        public price_per_hour: number,
        public student_count: number
    ) {

    }
}
