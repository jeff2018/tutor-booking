import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { Observable} from 'rxjs/Rx';
import { environment } from '../../../environments/environment';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

import { StudentCriteria } from '../../shared/models/student-criteria';
import { StudentResult } from '../../shared/models/student-result';
import { Student } from '../../shared/models/Student';

const httpOptions = { 
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class StudentService {

  private StudentRootApiUrl  = `${environment.ApiUrl}/api/Students`;
  private serachStudentApiURL = this.StudentRootApiUrl + "/search";
  
  constructor(private httpClient: HttpClient, 
    private toastyService: ToastyService, 
    private toastyConfig: ToastyConfig) {

  }

  public addStudent (Student: Student): Observable<Student> {
    console.log(Student);
    console.log(this.StudentRootApiUrl);
    return this.httpClient.post<Student>(this.StudentRootApiUrl, Student, httpOptions)
      .pipe(
        catchError(this.handleError('addStudent', Student))
      );
  }

  public updateStudent (Student: Student): Observable<Student> {
    console.log(Student);
    console.log(this.StudentRootApiUrl);
    return this.httpClient.put<Student>(this.StudentRootApiUrl, Student, httpOptions)
      .pipe(
        catchError(this.handleError('updateStudent', Student))
      );
  }

  public deleteStudent (Student: Student): Observable<Student> {
    console.log(Student);
    console.log(Student.id);
    console.log(this.StudentRootApiUrl);
    const deleteUrl = `${this.StudentRootApiUrl}/${Student.id}`; // DELETE api/Students/1
    console.log(deleteUrl);
    return this.httpClient.delete<Student>(deleteUrl, httpOptions)
      .pipe(
        catchError(this.handleError('deleteStudent', Student))
      );
  }

  public searchStudents(model) : Observable<StudentResult[]> {
    var getURL = `${this.serachStudentApiURL}?keyword=${model.keyword}&searchType=${model.searchType}&sortBy=${model.sortBy}&currentPerPage=${model.currentPerPage}&itemsPerPage=${model.itemsPerPage}`;
    return this.httpClient.get<StudentResult[]>(getURL, httpOptions)
      .pipe(catchError(this.handleError<StudentResult[]>('searchStudents')));

  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.addToastMessage("Error", JSON.stringify(error.error));
      return Observable.throw(error  || 'backend server error');
    };
  }

  addToastMessage(title, msg) {
    let toastOptions: ToastOptions = {
        title: title,
        msg: msg,
        showClose: true,
        timeout: 3500,
        theme: 'bootstrap',
        onAdd: (toast: ToastData) => {
            console.log('Toast ' + toast.id + ' has been added!');
        },
        onRemove: function(toast: ToastData) {
            console.log('Toast ' + toast.id + ' has been removed!');
        }
    };
    this.toastyService.error(toastOptions);
  }

}