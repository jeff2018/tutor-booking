import { Injectable } from '@angular/core';

import { HttpClient, 
        HttpHeaders, 
        HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { Observable} from 'rxjs/Rx';
import { environment } from '../../../environments/environment';
import { ToastyService, 
        ToastyConfig, 
        ToastOptions, 
        ToastData} from 'ng2-toasty';

import { TutorCriteria } from '../../shared/models/tutor-criteria';
import { TutorResult } from '../../shared/models/tutor-result';
import { Tutor } from '../../shared/models/tutor';

const httpOptions = { 
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class TutorService {

  private TutorRootApiUrl  = `${environment.ApiUrl}/api/tutors`;
  private serachTutorApiURL = this.TutorRootApiUrl + "/search";
  
  constructor(private httpClient: HttpClient, 
    private toastyService: ToastyService, 
    private toastyConfig: ToastyConfig) {

  }

  public addTutor (Tutor: Tutor): Observable<Tutor> {
    console.log(Tutor);
    console.log(this.TutorRootApiUrl);
    return this.httpClient.post<Tutor>(this.TutorRootApiUrl, Tutor, httpOptions)
      .pipe(
        catchError(this.handleError('addTutor', Tutor))
      );
  }

  public updateTutor (Tutor: Tutor): Observable<Tutor> {
    console.log(Tutor);
    console.log(this.TutorRootApiUrl);
    return this.httpClient.put<Tutor>(this.TutorRootApiUrl, Tutor, httpOptions)
      .pipe(
        catchError(this.handleError('updateTutor', Tutor))
      );
  }

  public deleteTutor (Tutor: Tutor): Observable<Tutor> {
    console.log(Tutor);
    console.log(Tutor.id);
    console.log(this.TutorRootApiUrl);
    const deleteUrl = `${this.TutorRootApiUrl}/${Tutor.id}`; // DELETE api/Tutors/1
    console.log(deleteUrl);
    return this.httpClient.delete<Tutor>(deleteUrl, httpOptions)
      .pipe(
        catchError(this.handleError('deleteTutor', Tutor))
      );
  }

  public searchTutors(model) : Observable<TutorResult[]> {
    var getURL = `${this.serachTutorApiURL}?keyword=${model.keyword}&searchType=${model.searchType}&sortBy=${model.sortBy}&currentPerPage=${model.currentPerPage}&itemsPerPage=${model.itemsPerPage}`;
    return this.httpClient.get<TutorResult[]>(getURL, httpOptions)
      .pipe(catchError(this.handleError<TutorResult[]>('searchTutors')));

  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.addToastMessage("Error", JSON.stringify(error.error));
      return Observable.throw(error  || 'backend server error');
    };
  }

  addToastMessage(title, msg) {
    let toastOptions: ToastOptions = {
        title: title,
        msg: msg,
        showClose: true,
        timeout: 3500,
        theme: 'bootstrap',
        onAdd: (toast: ToastData) => {
            console.log('Toast ' + toast.id + ' has been added!');
        },
        onRemove: function(toast: ToastData) {
            console.log('Toast ' + toast.id + ' has been removed!');
        }
    };
    this.toastyService.error(toastOptions);
  }

}
