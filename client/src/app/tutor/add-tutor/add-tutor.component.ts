import { Component, OnInit } from '@angular/core';

import { Tutor } from '../../shared/models/tutor';
import { TutorService } from '../../shared/services/tutor.service';

import { Observable } from 'rxjs/Observable';
import { ToastyService, 
        ToastyConfig,
        ToastOptions, 
        ToastData } from 'ng2-toasty';

import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-add-tutor',
  templateUrl: './add-tutor.component.html',
  styleUrls: ['./add-tutor.component.css']
})

export class AddTutorComponent implements OnInit {

  tutorsObservable: Observable<Tutor>;
  model = new Tutor('', '', '', 0, 0, 0, 0);

  constructor(private TutorService: TutorService,
    private toastyService: ToastyService, 
    private toastyConfig: ToastyConfig,
  private authService: AuthService) { 

   }

  ngOnInit() {
    this.authService.setLogon(true);
  }

  onSaveTutor() {
    console.log("Add new tutor !");
    console.log(JSON.stringify(this.model));
    console.log(this.model);
    this.tutorsObservable = this.TutorService.addTutor(this.model)
      .map(result => result);
    this.tutorsObservable
      .subscribe(Tutor => this.addToastMessage
        ("Added Tutor.", this.model.name));
  }

  addToastMessage(title, msg) {
    let toastOptions: ToastOptions = {
      title: title,
      msg: msg,
      showClose: true,
      timeout: 4500,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        console.log('Tutor ' + toast.id + ' has been added!');
      }
    };
    this.toastyService.success(toastOptions);
  }

}
