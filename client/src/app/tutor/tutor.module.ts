import { NgModule, ModuleWithProviders } from '@angular/core';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { BsDropdownModule, AlertModule, AccordionModule, 
        ButtonsModule, CollapseModule, PopoverModule, 
        ProgressbarModule, PaginationModule, ModalModule, 
        BsDatepickerModule, TabsModule, CarouselModule } from 'ngx-bootstrap';
import { SharedModule } from '../shared';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { AddTutorComponent } from './add-tutor/add-tutor.component';

import { TutorDetailsComponent } from './tutor-details/tutor-details.component';
import { TutorListComponent } from './tutor-list/tutor-list.component';
import { TutorSearchComponent } from './tutor-search/tutor-search.component';
import { TutorUploadComponent } from './tutor-upload/tutor-upload.component';


const tutorsRouting: ModuleWithProviders = RouterModule.forChild([
  { path: 'tutors/add',
    component: AddTutorComponent },
  { path: 'tutors',
    children: [
      { path: 'details',
        children: [
            { path: '',
              component: TutorDetailsComponent }, 
              ]},
      { path: 'list',
        children: [
            { path: '',
              component: TutorListComponent }, 
            ]},
      { path: '',
        component: TutorSearchComponent } ] },
  { path: 'tutors-details/:tutor_id',
    component: TutorDetailsComponent },
  { path: 'tutor-upload',
    component: TutorUploadComponent}
]);

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    tutorsRouting,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TabsModule.forRoot(),
    AccordionModule.forRoot(),
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    PopoverModule.forRoot(),
    ProgressbarModule.forRoot(),
    BrowserAnimationsModule,
    PaginationModule.forRoot(),
    ReactiveFormsModule
  ],
  declarations: [
    TutorDetailsComponent, 
    TutorUploadComponent, 
    AddTutorComponent, 
    TutorListComponent, 
    TutorSearchComponent
  ]
})
export class TutorModule { }
