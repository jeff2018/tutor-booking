import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';
import { TutorfirebaseService } from '../../shared/services/tutorfirebase.service';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';


@Component({
  selector: 'app-tutor-list',
  templateUrl: './tutor-list.component.html',
  styleUrls: ['./tutor-list.component.css']
})
export class TutorListComponent implements OnInit {
  results: any;

  constructor( private authService: AuthService,
              private tutorfirebase: TutorfirebaseService,
              private toastyService: ToastyService, 
              private toastyConfig: ToastyConfig) { }

  ngOnInit() {
    this.authService.setLogon(true);
    this.tutorfirebase.getAllTutorDetails().subscribe((result)=>{
      this.results = result;
    });
  }

}
