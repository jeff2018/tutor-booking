import { Component, OnInit, TemplateRef } from '@angular/core';

import { TutorCriteria } from '../../shared/models/tutor-criteria';
import { TutorResult } from '../../shared/models/tutor-result';
import { TutorService } from '../../shared/services/tutor.service';
import { Observable } from 'rxjs/Observable';
import { Tutor } from '../../shared/models/tutor';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ToastyService, ToastyConfig, ToastOptions, ToastData } from 'ng2-toasty';

import { environment } from '../../../environments/environment';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-tutor-search',
  templateUrl: './tutor-search.component.html',
  styleUrls: ['./tutor-search.component.css']
})

export class TutorSearchComponent implements OnInit {
  searchTypes = [{ desc: "subject", value: "subject" }, { desc: "name", value: "name" }, { desc: "location", value: "location" }];
  tutorsObservable$: Observable<TutorResult[]>;
  updateTutorObservable$: Observable<Tutor>;
  deleteTutorObservable$: Observable<Tutor>;

  modalRef: BsModalRef;
  result: TutorResult[] = [];
  private editTutor: Tutor;
  maxSize: number = 5;
  totalItems: number = 0;
  currentPage: number = 1;
  numPages: number = 0;
  inited: boolean = false;
  itemsPerPage: number = +environment.itemPerPage;
  indexOnPage: number = 0;
  model = new TutorCriteria('', 'subject', this.currentPage, this.itemsPerPage);

  constructor(private tutorService: TutorService,
    private modalService: BsModalService,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig,
    private authService: AuthService) {
      this.tutorsObservable$ = this.tutorService.searchTutors(this.model);
    }

  ngOnInit() {
    this.authService.setLogon(true);
    console.log(this.tutorsObservable$);
    this.tutorsObservable$.subscribe((x) => {
      this.totalItems = x.length;
      this.result = x.slice(this.indexOnPage, this.itemsPerPage);
    });
  }

  onSearch() {
    console.log(this.model.keyword);
    this.tutorsObservable$ = this.tutorService.searchTutors(this.model)
      .do(result => this.totalItems = result.length)
      .map(result => result);
    this.tutorsObservable$.subscribe(Tutors => this.result = Tutors);
  }

  pageChanged(event): void {
    console.log('Page changed to: ' + event.page);
    console.log('Number items per page: ' + event.itemsPerPage);
    this.model.currentPerPage = event.page;
    this.model.itemsPerPage = event.itemsPerPage;
    this.indexOnPage = event.page * (this.itemsPerPage);
    this.tutorsObservable$ = this.tutorService.searchTutors(this.model)
      .do(result => {
        this.totalItems = result.length;
        const numPages = result.length / this.itemsPerPage;
        console.log(numPages);
        if (numPages > 1 && this.model.currentPerPage > 1) {
          console.log(result);
          const startIndex = (this.indexOnPage - this.itemsPerPage);
          console.log(this.indexOnPage);
          const endIndex = this.indexOnPage;
          console.log(`<< ${startIndex} `);
          console.log(`< ${endIndex}`);
          this.result = result.slice(startIndex, endIndex);
          console.log(this.result);
        } else {
          console.log('page 1  > ' + event.page);
          this.result = result.slice(0, +environment.itemPerPage);
          console.log('page 1  > ' + this.result);
        }
        return this.result;
      })
      .map(result => result);
    this.tutorsObservable$.subscribe();
    console.log('this.TutorsObservable: ' + this.tutorsObservable$);
  }

  edit(tutorResult: TutorResult, template: TemplateRef<any>, index) {
    console.log(tutorResult);
    this.editTutor = new Tutor(tutorResult.location,
      tutorResult.name,
      tutorResult.subject,
      tutorResult.review,
      index,
      tutorResult.id);
    this.modalRef = this.modalService.show(template);
  }

  delete(tutorResult: TutorResult, template: TemplateRef<any>, index) {
    console.log(tutorResult);
    this.editTutor = new Tutor(tutorResult.location,
      tutorResult.name,
      tutorResult.subject,
      tutorResult.review,
      index,
      tutorResult.id);
    this.modalRef = this.modalService.show(template);
  }

  onSaveEditTutor() {
    console.log("Save Edited Tutor");
    this.updateTutorObservable$ = this.tutorService.updateTutor(this.editTutor);
    this.updateTutorObservable$.subscribe(Tutor => {
      this.addToastMessage("Update Tutor.", this.editTutor.subject);
      var TutorRsObj = this.result[this.editTutor.student_count];
      TutorRsObj.location = this.editTutor.location;
      TutorRsObj.name = this.editTutor.name;
      TutorRsObj.subject = this.editTutor.subject;
      this.result[this.editTutor.student_count] = TutorRsObj;
      this.modalRef.hide();
    });
  }

  onDelTutor() {
    console.log("Delete Tutor !" + this.editTutor.student_count);
    this.deleteTutorObservable$ = this.tutorService.deleteTutor(this.editTutor);
    this.deleteTutorObservable$.subscribe(Tutor => {
      this.addToastMessage("Tutor deleted.", this.editTutor.subject);
      console.log(this.editTutor.student_count);
      let TutorRsObj = this.result[this.editTutor.student_count];
      let index = this.result.indexOf(TutorRsObj, 0);
      if (index > -1) {
        this.result.splice(index, 1);
      }
      console.log(this.result[this.editTutor.student_count]);
      this.modalRef.hide();
    });
  }

  addToastMessage(title, msg) {
    let toastOptions: ToastOptions = {
      title: title,
      msg: msg,
      showClose: true,
      timeout: 4500,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        console.log('Tutor ' + toast.id + ' has been updated!');
      }
    };
    this.toastyService.success(toastOptions);
  }

}
