import { BrowserModule } from '@angular/platform-browser';

import {
  NgModule,
  ModuleWithProviders
} from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { ToastyModule } from 'ng2-toasty';
import { NgxLocalStorageModule } from 'ngx-localstorage';

import { firebaseConfig } from "../environments/firebase.config";
import { AngularFireModule } from "angularfire2";
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';

import { AngularFireAuthModule } from 'angularfire2/auth';
import { HomeComponent } from './home/home.component';
import { SecurityModule } from './security';
import { SharedModule } from './shared';
import {
  TutorService,
  AuthService,
  FileuploadService,
  TutorfirebaseService,
} from './shared/services';
import { FooterComponent } from './shared/footer/footer.component';
import { HeaderComponent } from './shared/header/header.component';
import { TutorModule } from './tutor';
import { HomeModule } from './home/home.module';
import { RegisterComponent } from './security/register/register.component';

const rootRouting: ModuleWithProviders = RouterModule.forRoot([
  {
    path: 'Home',
    redirectTo: '/'
  },
  {
    path: 'register',
    component: RegisterComponent
  }
]);

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    rootRouting,
    ToastyModule,
    HomeModule,
    SharedModule,
    TutorModule,
    SecurityModule,
    ToastyModule,
    NgxLocalStorageModule.forRoot()
  ],
  providers: [
    TutorService,
    AuthService,
    FileuploadService,
    TutorfirebaseService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
