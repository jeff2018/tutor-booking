import { NgModule, ModuleWithProviders } from '@angular/core';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { BsDropdownModule, AlertModule, AccordionModule, 
        ButtonsModule, CollapseModule, PopoverModule, 
        ProgressbarModule, PaginationModule, ModalModule, 
        BsDatepickerModule, TabsModule, CarouselModule } from 'ngx-bootstrap';
import { SharedModule } from '../shared';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { AddStudentComponent } from './add-student/add-student.component';
import { StudentDetailsComponent } from './student-details/student-details.component';
import { StudentListComponent } from './student-list/student-list.component';
import { StudentSearchComponent } from './student-search/student-search.component';
import { StudentUploadComponent } from './student-upload/student-upload.component';

const studentsRouting: ModuleWithProviders = RouterModule.forChild([
  { path: 'students/add',
    component: AddStudentComponent },
  { path: 'students',
    children: [
      { path: 'details',
        children: [
            { path: '',
              component: StudentDetailsComponent }, 
              ]},
      { path: 'list',
        children: [
            { path: '',
              component: StudentListComponent }, 
            ]},
      { path: '',
        component: StudentSearchComponent } ] },
  { path: 'students-details/:student_id',
    component: StudentDetailsComponent },
  { path: 'student-upload',
    component: StudentUploadComponent}
]);
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    studentsRouting,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TabsModule.forRoot(),
    AccordionModule.forRoot(),
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    PopoverModule.forRoot(),
    ProgressbarModule.forRoot(),
    BrowserAnimationsModule,
    PaginationModule.forRoot(),
    ReactiveFormsModule
  ],
  declarations: [AddStudentComponent, 
    StudentDetailsComponent, 
    StudentListComponent, 
    StudentSearchComponent, 
    StudentUploadComponent]
})
export class StudentModule { }
