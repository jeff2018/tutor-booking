import { Component, OnInit } from '@angular/core';

import { Student } from '../../shared/models/student';
import { StudentService } from '../../shared/services/student.service';

import { Observable } from 'rxjs/Observable';
import { ToastyService, 
        ToastyConfig,
        ToastOptions, 
        ToastData } from 'ng2-toasty';

import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {

  StudentsObservable: Observable<Student>;
  model = new Student(0, '', '','', 0);

  constructor(private StudentService: StudentService,
    private toastyService: ToastyService, 
    private toastyConfig: ToastyConfig,
  private authService: AuthService) { 

   }

  ngOnInit() {
    this.authService.setLogon(true);
  }

  onSaveStudent() {
    console.log("Add new Student !");
    console.log(JSON.stringify(this.model));
    console.log(this.model);
    this.StudentsObservable = this.StudentService.addStudent(this.model)
      .map(result => result);
    this.StudentsObservable
      .subscribe(Student => this.addToastMessage
        ("Added Student.", this.model.name));
  }

  addToastMessage(title, msg) {
    let toastOptions: ToastOptions = {
      title: title,
      msg: msg,
      showClose: true,
      timeout: 4500,
      theme: 'bootstrap',
      onAdd: (toast: ToastData) => {
        console.log('Student ' + toast.id + ' has been added!');
      }
    };
    this.toastyService.success(toastOptions);
  }

}
