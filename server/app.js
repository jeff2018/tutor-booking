require('dotenv').config() //firebase included
const express = require("express");
const mysql = require("mysql");
const bodyParser = require("body-parser");
const q = require("q");
const cors = require('cors');
const multer = require('multer');
const googleStorage = require('@google-cloud/storage');
var app = express();

const admin = require('firebase-admin');//firebase
const keys = require('./jgyy.json');
const path = require('path');

//configure
admin.initializeApp({
    databaseURL: 'https://jgyy-0.firebaseio.com/',
    credential: admin.credential.cert(keys)
});

const bucket = admin.storage().bucket('jgyy-0.appspot.com');
const upload = multer({ dest: path.join(__dirname, 'tmp') });

const gstorage = googleStorage({
    projectId: 'jgyy-0',
    keyFileName: process.env.FIREBASE_KEYFILENAME
});

const googleMulter = multer({
    storage: multer.memoryStorage(),
    limits: {
        fileSize: 20 * 1024 * 1024 // 20MB
    }
})

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '_' + file.originalname)
    }
})

let diskUpload = multer({ storage: storage });

// Import and mount the crudRouter
const crudRouter = require('./crud.js');

app.use(cors()) //Express JS INIT
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));

app.use('/api', crudRouter);

app.post('/upload-firestore', googleMulter.single('coverThumbnail'), (req, res) => {
    console.log('upload here ...');
    console.log(req.file);//3 query
    uploadToFireBaseStorage(req.file).then((result => {
        console.log("firebase stored -> " + result);
        saveGallery([req.file.originalnamename, result, req.body.remarks]).then((result) => {
            console.log(result);
        }).catch((error) => {
            console.log("error ->" + error);
        })
    })).catch((error) => {
        console.log(error);
    })
    res.status(200).json({});
})

app.post('/upload', diskUpload.single('coverThumbnail'), (req, res) => {
    console.log('upload here ...');
    res.status(200).json(req.file);
})

const uploadToFireBaseStorage = function (file) {
    return new Promise((resolve, reject) => {
        if (!file) { // upload the incoming file from the angular5 ui 
            reject('Invalid file upload');
        }
        let newfileName = `${Date.now()}_${file.originalname}`;
        let fileupload = bucket.file(newfileName);
        const blobStream = fileupload.createWriteStream({
            metadata: {
                contentType: file.mimetype
            }
        });
        blobStream.on('error', (error) => {
            console.log(error);
            reject('Something went wrong during file upload');
        });
        blobStream.on('finish', () => {
            console.log(fileupload.name);
            const url = `https://firebasestorage.googleapis.com/v0/b/jgyy-0.appspot.com/o/${fileupload.name}?alt=media`;
            file.fileURL = url; 
            resolve(url);
        });
        blobStream.end(file.buffer);
    });
}

app.use(express.static(__dirname + "/public"));

app.listen(3000, function () { //localhost:3000
    console.info("App server started on port " + 3000);
});