//second refactor attempt. please work!
const express = require("express");
const mysql = require("mysql");
const bodyParser = require("body-parser");
const q = require("q");
const cors = require('cors');
const multer = require('multer');
const googleStorage = require('@google-cloud/storage');

const { findAll, findOne, deleteOne, deleteOneStudent, updateOne, saveOne, saveGallery, findAllStudent, 
    findOneStudent, searchTutors, searchByNames, searchBySubject, updateOneStudent } = require('./jeffsql.js');

let tutoring = [];
crudRouter = express.Router();

crudRouter.get("/tutors", function (req, res) {
    console.log(req.query.limit, req.query.offset);
    let limit = parseInt(req.query.limit) || 50;
    let offset = parseInt(req.query.offset) || 0;
    findAll([limit, offset]) //2 query
        .then(function (results) {
            res.status(200).json(results);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

crudRouter.get("/students", function (req, res) {
    console.log(req.query.limit, req.query.offset);
    let limit = parseInt(req.query.limit) || 50;
    let offset = parseInt(req.query.offset) || 0;
    findAllStudent([limit, offset]) //2 query
        .then(function (results) {
            res.status(200).json(results);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

crudRouter.get("/tutor/:tutorId", function (req, res) {
    console.log(req.params.TutorId);
    findOne([req.params.TutorId]) //1 query
        .then(function (results) {
            res.status(200).json(results[0]);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

crudRouter.get("/student/:studentId", function (req, res) {
    console.log(req.params.studentId);
    findOneStudent([req.params.studentId]) //1 query
        .then(function (results) {
            res.status(200).json(results[0]);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

crudRouter.post("/tutors", function (req, res) {
    console.log(req.body);//6 query
    saveOne([req.body.student_count, req.body.price_per_hour, req.body.review,
    req.body.subject, req.body.location, req.body.name])
        .then(function (results) {
            res.status(200).json(results);
            console.log(results);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

crudRouter.delete("/tutors/:delId", function (req, res) {
    console.log(req.body);
    res.status(200).json({});
    console.log(req.params.delId);//1 query
    deleteOne([req.params.delId])
        .then(function (result) {
            res.status(200).json(result);
            console.log(result);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

crudRouter.delete("/students/:delId", function (req, res) {
    console.log(req.body);
    res.status(200).json({});
    console.log(req.params.delId);//1 query
    deleteOneStudent([req.params.delId])
        .then(function (result) {
            res.status(200).json(result);
            console.log(result);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

crudRouter.put("/tutors", function (req, res) {
    console.log(req.body);//7 query
    updateOne([req.body.student_count, req.body.price_per_hour, req.body.review,
    req.body.subject, req.body.location, req.body.name, req.body.id])
        .then(function (result) {
            console.log(result);
            res.status(200).json(result);
        })
        .catch(function (err) {
            console.log(err);
            res.status(500).end();
        });
});

crudRouter.put("/tutors", function (req, res) {
    console.log(req.body);//4 query
    updateOneStudent([req.body.scores, req.body.level, req.body.subject,
                        req.body.name, req.body.id])
        .then(function (result) {
            console.log(result);
            res.status(200).json(result);
        })
        .catch(function (err) {
            console.log(err);
            res.status(500).end();
        });
});

crudRouter.get("/tutors/search", function (req, res) { //search function
    console.log(req.query);
    var searchType = req.query.searchType;
    var keyword = req.query.keyword;
    if (typeof searchType === 'string' && searchType != '1') {
        if (searchType == 'Name') {
            console.log('search by names');
            var name = "%" + keyword + "%";
            searchByNames([name])
                .then(function (results) {
                    res.status(200).json(results);
                    console.log(results);
                })
                .catch(function (err) {
                    console.log(err);
                    res.status(500).end();
                });
        } else {
            console.log('search by location and subject');
            var location = "%" + keyword + "%";
            var subject = "%" + keyword + "%";
            searchBySubject([location, subject])
                .then(function (results) {
                    res.status(200).json(results);
                    console.log(results);
                })
                .catch(function (err) {
                    console.log(err);
                    res.status(500).end();
                });
        }
    } else {
        console.log('search by both');
        var name = "%" + keyword + "%";
        var location = "%" + keyword + "%";
        var subject = "%" + keyword + "%";
        console.log(name, location, subject);
        searchTutors([name, location, subject])
            .then(function (results) {
                res.status(200).json(results);
                console.log(results);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            });
    }
});

module.exports = crudRouter;