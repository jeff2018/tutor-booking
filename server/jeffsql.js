//attempt to refactor

require('dotenv').config();
const bodyParser = require("body-parser");
const mysql = require("mysql");
const q = require("q");

let pool = mysql.createPool({ //MYSQL
    host: process.env.MYSQL_SERVER,
    port: process.env.MYSQL_PORT,
    user: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    connectionLimit: process.env.MYSQL_CONNECTION
});

const makeQuery = function (sql, pool) { //query from SQL
    console.log(sql);
    return function (args) {
        var defer = q.defer();
        pool.getConnection(function (err, conn) {
            if (err) {
                defer.reject(err);
                return;
            }
            conn.query(sql, args || [], function (err, results) {
                conn.release();
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve(results);
            });
        });
        return defer.promise;
    }
};

const findAllTutors = `SELECT id, name, location, subject, review, price_per_hour, 
                        student_count FROM tutors LIMIT ? OFFSET ?`;
const findAll = makeQuery(findAllTutors, pool);//2 query

const findAllStudents = `SELECT id, name, subject, level, scores FROM students LIMIT ? OFFSET ?`;
const findAllStudent = makeQuery(findAllStudents, pool);//2 query

const findOneTutor = "SELECT * FROM tutors WHERE id = ?";
const findOne = makeQuery(findOneTutor, pool);//1 query

const findOneStudents = "SELECT * FROM students WHERE id = ?";
const findOneStudent = makeQuery(findOneStudents, pool);//1 query

const deleteOneTutor = "DELETE FROM tutors WHERE id = ?";
const deleteOne = makeQuery(deleteOneTutor, pool);//1 query

const deleteOneStudents = "DELETE FROM students WHERE id = ?";
const deleteOneStudent = makeQuery(deleteOneStudents, pool);//1 query

const updateTutor = `UPDATE tutors SET student_count = ?, price_per_hour = ?, 
                    review = ?, subject = ?, location = ?, name = ?,  WHERE id = ?`; //error, ID not set
const updateOne = makeQuery(updateTutor, pool);//7 query

const updateStudents = `UPDATE students SET scores = ?, level = ?, 
                    subject = ?, name = ? WHERE id = ?`; //error, ID not set
const updateOneStudent = makeQuery(updateStudents, pool);//5 query

const saveOneTutor = `INSERT INTO tutors (student_count, price_per_hour, review, 
                        subject, location, name) VALUES (?, ?, ?, ?, ?, ?)`;
const saveOne = makeQuery(saveOneTutor, pool);//6 query

const saveOneGallery = "INSERT INTO gallery (filename, fileUrl, remarks) VALUES (? ,? ,?)";
const saveGallery = makeQuery(saveOneGallery, pool);//3 query

const searchTutorsByCriteria = "SELECT * FROM tutors WHERE (name LIKE ?) || location LIKE ? || subject LIKE ?";
const searchTutors = makeQuery(searchTutorsByCriteria, pool);//3 query

const searchTutorByNames = "SELECT * FROM tutors WHERE name LIKE ?";
const searchByNames = makeQuery(searchTutorByNames, pool);//1 query

const searchTutorBySubject = "SELECT * FROM tutors WHERE location LIKE ? || subject LIKE ?";
const searchBySubject = makeQuery(searchTutorBySubject, pool);//2 query

module.exports = {
    findAll, findOne, deleteOne, updateOne, deleteOneStudent,
    saveOne, saveGallery, findAllStudent, findOneStudent,
    searchTutors, searchByNames, searchBySubject, updateOneStudent
};