require('dotenv').config() //firebase included
const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const q = require("q");
const cors = require('cors');
const multer = require('multer');
const googleStorage = require('@google-cloud/storage');
var app = express();

const admin = require('firebase-admin');//firebase
const keys = require('./jgyy.json');
const path = require('path');

//configure
admin.initializeApp({
    databaseURL: 'https://jgyy-0.firebaseio.com/',
    credential: admin.credential.cert(keys)
});

const bucket = admin.storage().bucket('jgyy-0.appspot.com');
const upload = multer({ dest: path.join(__dirname, 'tmp') });

const gstorage = googleStorage({
    projectId: 'jgyy-0',
    keyFileName: process.env.FIREBASE_KEYFILENAME
});

const googleMulter = multer({
    storage: multer.memoryStorage(),
    limits: {
        fileSize: 20 * 1024 * 1024 // 20MB
    }
})

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '_' + file.originalname)
    }
})

var diskUpload = multer({ storage: storage })

app.use(cors()) //Express JS INIT
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb' }));

var pool = mysql.createPool({ //MYSQL
    host: process.env.MYSQL_SERVER,
    port: process.env.MYSQL_PORT,
    user: process.env.MYSQL_USERNAME,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    connectionLimit: process.env.MYSQL_CONNECTION
});

const NODE_PORT = process.env.PORT;

const makeQuery = function (sql, pool) {
    console.log(sql);
    return function (args) {
        var defer = q.defer();
        pool.getConnection(function (err, conn) {
            if (err) {
                defer.reject(err);
                return;
            }
            conn.query(sql, args || [], function (err, results) {
                conn.release();
                if (err) {
                    defer.reject(err);
                    return;
                }
                defer.resolve(results);
            });
        });
        return defer.promise;
    }
};

const findAllTutors = `SELECT id, name, location, subject, review, price_per_hour, 
                        student_count FROM tutors LIMIT ? OFFSET ?`;
const findAll = makeQuery(findAllTutors, pool);//2 query
const findOneTutor = "SELECT * FROM tutors WHERE id = ?";
const findOne = makeQuery(findOneTutor, pool);//1 query
const deleteOneTutor = "DELETE FROM tutors WHERE id = ?";
const deleteOne = makeQuery(deleteOneTutor, pool);//1 query
const updateTutor = `UPDATE tutors SET student_count = ?, price_per_hour = ?, 
                    review = ?, subject = ?, location = ?, name = ?,  WHERE id = ?`;
const updateOne = makeQuery(updateTutor, pool);//7 query
const saveOneTutor = `INSERT INTO tutors (student_count, price_per_hour, review, 
                        subject, location, name) VALUES (?, ?, ?, ?, ?, ?)`;
const saveOne = makeQuery(saveOneTutor, pool);//6 query
const saveOneGallery = "INSERT INTO gallery (filename, fileUrl, remarks) VALUES (? ,? ,?)";
const saveGallery = makeQuery(saveOneGallery, pool);//3 query

app.get("/api/tutors", function (req, res) {
    console.log(req.query.limit, req.query.offset);
    let limit = parseInt(req.query.limit) || 50;
    let offset = parseInt(req.query.offset) || 0;
    findAll([limit, offset]) //2 query
        .then(function (results) {
            res.status(200).json(results);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

const findAllStudents = `SELECT id, name, subject, level, scores FROM students LIMIT ? OFFSET ?`;
const findAllStudent = makeQuery(findAllTutors, pool);//2 query

app.get("/api/students", function (req, res) {
    console.log(req.query.limit, req.query.offset);
    let limit = parseInt(req.query.limit) || 50;
    let offset = parseInt(req.query.offset) || 0;
    findAllStudent([limit, offset]) //2 query
        .then(function (results) {
            res.status(200).json(results);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

app.get("/api/tutor/:tutorId", function (req, res) {
    console.log(req.params.TutorId);
    findOne([req.params.TutorId]) //1 query
        .then(function (results) {
            res.status(200).json(results[0]);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

const findOneStudents = "SELECT * FROM students WHERE id = ?";
const findOneStudent = makeQuery(findOneStudents, pool);//1 query

app.get("/api/student/:studentId", function (req, res) {
    console.log(req.params.studentId);
    findOneStudent([req.params.studentId]) //1 query
        .then(function (results) {
            res.status(200).json(results[0]);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

app.post("/api/tutors", function (req, res) {
    console.log(req.body);//6 query
    saveOne([req.body.student_count, req.body.price_per_hour, req.body.review,
    req.body.subject, req.body.location, req.body.name])
        .then(function (results) {
            res.status(200).json(results);
            console.log(results);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

app.delete("/api/tutors/:delId", function (req, res) {
    console.log(req.body);
    res.status(200).json({});
    console.log(req.params.delId);//1 query
    deleteOne([req.params.delId])
        .then(function (result) {
            res.status(200).json(result);
            console.log(result);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});

app.put("/api/tutors", function (req, res) {
    console.log(req.body);//7 query
    updateOne([req.body.student_count, req.body.price_per_hour, req.body.review,
    req.body.subject, req.body.location, req.body.name, req.body.id])
        .then(function (result) {
            console.log(result);
            res.status(200).json(result);
        })
        .catch(function (err) {
            console.log(err);
            res.status(500).end();
        });
});

const searchTutorsByCriteria = "SELECT * FROM tutors WHERE (name LIKE ?) || location LIKE ? || subject LIKE ?";
const searchTutors = makeQuery(searchTutorsByCriteria, pool);//3 query
const searchTutorByNames = "SELECT * FROM tutors WHERE name LIKE ?";
const searchByNames = makeQuery(searchTutorByNames, pool);//1 query
const searchTutorBySubject = "SELECT * FROM tutors WHERE location LIKE ? || subject LIKE ?";
const searchBySubject = makeQuery(searchTutorBySubject, pool);//2 query

//search function
app.get("/api/tutors/search", function (req, res) {
    console.log(req.query);
    var searchType = req.query.searchType;
    var keyword = req.query.keyword;
    if (typeof searchType === 'string' && searchType != '1') {
        if (searchType == 'Name') {
            console.log('search by names');
            var name = "%" + keyword + "%";
            searchByNames([name])
                .then(function (results) {
                    res.status(200).json(results);
                    console.log(results);
                })
                .catch(function (err) {
                    console.log(err);
                    res.status(500).end();
                });
        } else {
            console.log('search by location and subject');
            var location = "%" + keyword + "%";
            var subject = "%" + keyword + "%";
            searchBySubject([location, subject])
                .then(function (results) {
                    res.status(200).json(results);
                    console.log(results);
                })
                .catch(function (err) {
                    console.log(err);
                    res.status(500).end();
                });
        }
    } else {
        console.log('search by both');
        var name = "%" + keyword + "%";
        var location = "%" + keyword + "%";
        var subject = "%" + keyword + "%";
        console.log(name, location, subject);
        searchTutors([name, location, subject])
            .then(function (results) {
                res.status(200).json(results);
                console.log(results);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            });
    }
});

app.post('/upload-firestore', googleMulter.single('coverThumbnail'), (req, res) => {
    console.log('upload here ...');
    console.log(req.file);//3 query
    uploadToFireBaseStorage(req.file).then((result => {
        console.log("firebase stored -> " + result);
        saveGallery([req.file.originalnamename, result, req.body.remarks]).then((result) => {
            console.log(result);
        }).catch((error) => {
            console.log("error ->" + error);
        })
    })).catch((error) => {
        console.log(error);
    })
    res.status(200).json({});
})

app.post('/upload', diskUpload.single('coverThumbnail'), (req, res) => {
    console.log('upload here ...');
    res.status(200).json(req.file);
})

const uploadToFireBaseStorage = function (file) {
    return new Promise((resolve, reject) => {
        if (!file) { // upload the incoming file from the angular5 ui 
            reject('Invalid file upload');
        }
        let newfileName = `${Date.now()}_${file.originalname}`;
        let fileupload = bucket.file(newfileName);
        const blobStream = fileupload.createWriteStream({
            metadata: {
                contentType: file.mimetype
            }
        });
        blobStream.on('error', (error) => {
            console.log(error);
            reject('Something went wrong during file upload');
        });
        blobStream.on('finish', () => {
            console.log(fileupload.name);
            const url = `https://firebasestorage.googleapis.com/v0/b/jgyy-0.appspot.com/o/${fileupload.name}?alt=media`;
            file.fileURL = url; 
            resolve(url);
        });
        blobStream.end(file.buffer);
    });
}

app.use(express.static(__dirname + "/public"));

app.listen(3000, function () {
    console.info("App server started on port " + 3000);
});