# TUTOR BOOKING WEB APPLICATION

This project was generated with Angular 5, express.js 4, node.js 5, MYSQL 5 and google firebase

## Updates

This readme will be updated by jeffrey goh over time.
The default readme at client portion have been deleted.

Dummy login and register have been placed with zero authentications.
By Assignment 4 I am intending to let firebase be the server instead of writing express server.
## Databsse

Refer to "student-teacher.sql" for the database. Database will be based on MYSQL and storage uses Google FireBase.

My firebase storage is located at "gs://jgyy-0.appspot.com"

My real time database is located at "https://jgyy-0.firebaseio.com/". I will be using both Realtime Database and Cloud Firestore.

##Client and Server

Client and Server have been seperated into 2 different directory. For the server SQL side, there are 3 tables which are tutors, students and gallery!

For this assignment i will temporory share my private key and set read and write in firebase database as true.

To run the client, go to "cd client" and type "ng serve". Student component to be included.

To run the server, go to "cd server" and type "nodemon app.js". The server have refactored into 3 components.
